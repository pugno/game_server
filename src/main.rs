#![allow(dead_code, unreachable_code)]

mod network;
mod components;
mod spawn;
mod locations;
mod process_packets;
mod send_packets;
mod typeutils;

use specs::prelude::*;
use crate::components::*;
use crate::network::*;
use crate::locations::Locations;
use crate::process_packets::ProcessPackets;
//use crate::send_packets::SendPackets;

#[tokio::main]
async fn main() -> tokio::io::Result<()> {
    let mut world = World::new();
    world.register::<Position>();
    world.register::<Health>();
    world.register::<CharacterStats>();
    world.register::<ItemStats>();
    world.register::<Npc>();
    world.register::<Player>();
    world.register::<Item>();
    world.register::<NetPacket>();
    world.register::<BackPacket>();

    let mut dispatcher = DispatcherBuilder::new()
        .with(Locations, "Locations", &[])
        .with(ProcessPackets, "ProcessPackets", &[])
        //.with(SendPackets, "SendPackets", &[])
        .build();
    println!("initializing server...");
    let (packet_rx, update_tx) = initialize_server().await?;
    println!("server initialized. initalizing batch thread...");
    let mut batch_rx = batch_packets(packet_rx).await?;
    println!("batch thread initialized. Starting server...");
    loop {
        let packet_res = batch_rx.recv().await;
        match packet_res {
            Ok(packet_vec) => {
                println!("packets recieved from batch thread. handling...");
                println!("batched packet vec length: {:?}", packet_vec.len());
                for packet in packet_vec {
                    handle_packet(packet, &mut world);
                }
                println!("packets handled.")
            },
            Err(e) => {
                print!("nothing to recieve\n {:?}", e);
            }
        }
        println!("running systems...");
        dispatcher.dispatch(&mut world);
        world.maintain();

        println!("system cycle complete. returning packets...");
        //Send update packet to threads
        let delete_vec = return_packet(&mut world, &update_tx);
        for ent in delete_vec {
            let res = world.delete_entity(ent);
            match res {
                Ok(_t) => { },
                Err(_e) => println!("Wrong generation encountered!")
            }
        }
        println!("packets returned");
        world.maintain();
    }
    Ok(())
}
