use specs::prelude::*;
use crate::components::*;
pub struct Locations;

impl<'a> System<'a> for Locations {
    type SystemData = (ReadStorage<'a, Player>, ReadStorage<'a, Position>);

    fn run(&mut self, (player, pos): Self::SystemData) {
        for (player, pos) in (&player, &pos).join() {
            println!("Player {:?}'s position: {:?}, {:?}, {:?}", player.client_id, pos.x, pos.y, pos.z)
        }
    }
}