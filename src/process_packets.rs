use specs::prelude::*;
use crate::{components::*, network::PositionPacket};


pub struct ProcessPackets;

impl<'a> System<'a> for ProcessPackets {
    type SystemData = (WriteStorage<'a, NetPacket>, ReadStorage<'a, Player>, WriteStorage<'a, Position>, Entities<'a>, Read<'a, LazyUpdate>);

    fn run(&mut self, (mut packet, player, mut pos, ent, updater): Self::SystemData) {
        let mut packet_vec = Vec::new();
        let mut delete_vec = Vec::<Entity>::new();
        for (packet, e) in (&mut packet, &ent).join() {
            packet_vec.push(packet);
            delete_vec.push(e);
        }

        for (player, pos,) in (&player, &mut pos).join() {
            for packet in &packet_vec {
                if player.client_id == packet.client_id {
                    match packet.action {
                        Action::Attack(_a) => { println!("player {} attacks!", player.client_id) },
                        Action::Translate(m) => {
                            pos.x += m.dx;
                            pos.y += m.dy;
                            pos.z += m.dz;
                            println!("player {} moves!", player.client_id);
                            let back_packet = ent.create();
                            //println!("generating backpacket for entity {:?}", back_packet);
                            updater.insert(back_packet, BackPacket { player_id: player.client_id, action: Action::Position(PositionPacket { x: pos.x, y: pos.y, z: pos.z }), ent: back_packet});
                        },
                        Action::Refresh(_r) => {
                        },
                        Action::Pickup(_p) => { println!("player {} picks up an item!", player.client_id)},
                        Action::Position(_p) => unreachable!()
                    }
                }
            }
        }
        for e in delete_vec {
            updater.exec_mut(move |world| {
                let result = world.delete_entity(e);
                match result {
                    Ok(_t) => { },
                    Err(_e) => { println!("Wrong generation encountered!") }
                }
            })
        }
    } 
}