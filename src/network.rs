use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpListener;
use tokio::sync::broadcast;
use tokio::sync::broadcast::{Receiver, Sender};
use std::time::Instant;
use specs::prelude::*;

use crate::components::*;
use crate::spawn::spawn_player;

#[derive(Clone, Copy, Debug)]
pub enum PacketType {
    ServerReady,
    SpawnPlayer,
    Translate,
    Attack,
    Pickup,
    Refresh,
    RefreshPosition,
}

#[derive(Clone, Copy)]
pub union PacketContent {
    pub movement: MovementPacket,
    pub attack: AttackPacket,
    pub pickup: PickupPacket,
    pub refresh: RefreshPacket,
    pub spawn_player: SpawnPlayerPacket,
    pub position: PositionPacket,
}
#[derive(Clone, Copy, Debug)]
pub struct MovementPacket {
    pub dx: i32,
    pub dy: i32,
    pub dz: i32,
}
#[derive(Clone, Copy, Debug)]
pub struct PositionPacket {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

#[derive(Clone, Copy, Debug)]
pub struct AttackPacket;

#[derive(Clone, Copy, Debug)]
pub struct PickupPacket;

#[derive(Clone, Copy, Debug)]
pub struct RefreshPacket;

#[derive(Clone, Copy, Debug)]
pub struct SpawnPlayerPacket {
    pub position: Position,
    pub health: Health,
    pub stats: CharacterStats,
    pub player: Player
}

#[derive(Clone, Copy)]
pub struct Packet {
    pub typ: PacketType,
    pub content: PacketContent,
    pub client_id: i32,
}

pub async fn initialize_server<'a>() -> tokio::io::Result<(Receiver<Packet>, Sender<Packet>)> {

    let listener = TcpListener::bind("127.0.0.1:1776").await?;
    let (packet_tx, packet_rx) = broadcast::channel(128);
    let (update_tx, _update_rx) = broadcast::channel(128);
    let update_tx_copy = update_tx.to_owned();
    tokio::spawn(async move {
        handle_connections(listener, &packet_tx, &update_tx_copy)
            .await
            .expect("error handling connection");
    });

    Ok((packet_rx, update_tx))
}

pub async fn handle_connections(
    listener: TcpListener,
    packet_tx: &Sender<Packet>,
    update_tx: &Sender<Packet>,
) -> tokio::io::Result<()> {
    let mut client_id: i32 = 0;
    loop {
        let thread_tx = packet_tx.to_owned();
        let update_tx = update_tx.to_owned();
        let (socket, _) = listener.accept().await?;
        let (mut readstream, mut writestream) = tokio::io::split(socket);
        client_id += 1;
        //TODO: set up database to store player data and retrieve for spawn - coffee pls
        //TODO: auth token
        let _res = thread_tx.send(Packet {
            typ: PacketType::SpawnPlayer,
            content: PacketContent {
                spawn_player : SpawnPlayerPacket {
                    position: Position {
                        x: 0,
                        y: 0,
                        z: 0
                    },
                    health: Health {
                        health: 100
                    },
                    stats: CharacterStats {
                        level: 1,
                        strength: 1, 
                        constitution: 1, 
                        dexterity: 1, 
                        intelligence: 1, 
                        wisdom: 1, 
                        charisma: 1
                    },
                    player: Player {
                        client_id: client_id
                    }
                },
                },
                client_id: client_id
            }
        );
        tokio::spawn(async move {
            let id = client_id.to_owned();
            loop {
                let mut buffer: [u8; 128] = [0; 128];
                let _n = match readstream.read(&mut buffer).await {
                    Ok(n) if n == 0 => return,
                    Ok(n) => n,
                    Err(_e) => {
                        print!("error reading from socket");
                        return;
                    }
                };
                match buffer[0] {
                    1 => {
                        // movement packet
                        let content = MovementPacket {
                            dx: buffer[1] as i8 as i32,
                            dy: buffer[2] as i8 as i32,
                            dz: buffer[3] as i8 as i32,
                        };
                        let packet = Packet {
                            typ: PacketType::Translate,
                            content: PacketContent { movement: content },
                            client_id: id
                        };
                        let _res = thread_tx.send(packet);
                    },
                    2 => {
                        let content = AttackPacket;
                        let packet = Packet {
                            typ: PacketType::Attack,
                            content: PacketContent { attack: content },
                            client_id: id
                        };
                        let _res = thread_tx.send(packet);
                    },
                    3 => {
                        let content = PickupPacket;
                        let packet = Packet {
                            typ: PacketType::Pickup,
                            content: PacketContent { pickup: content },
                            client_id: id
                        };
                        let _res = thread_tx.send(packet);
                    }
                    4 => {
                        let content = RefreshPacket;
                        let packet = Packet {
                            typ: PacketType::Refresh,
                            content: PacketContent { refresh: content },
                            client_id: id
                        };
                        let _res = thread_tx.send(packet);
                    }
                    _ => {
                        println!("invalid packet recieved");
                    }
                }
                // TODO: batch recieve jobs and send packets as a vec
            }
        });
        tokio::spawn(async move {
            let mut thread_rx = update_tx.subscribe();
            loop {
                let refresh_packet = thread_rx.recv().await;
                match refresh_packet {
                    Ok(packet) => {
                        match packet.typ {
                            PacketType::RefreshPosition => {
                                let command_byte: &[u8] = &[1];
                                let term_sequence = &[4, 20, 69, 4, 20, 69]; 
                                let player_id = [packet.client_id as u8];
                                let player_x = unsafe { &packet.content.position.x.to_be_bytes() };
                                let player_y = unsafe { &packet.content.position.y.to_be_bytes() };
                                let player_z = unsafe { &packet.content.position.z.to_be_bytes() };
                                let u8_vec = [command_byte, term_sequence, &player_id, term_sequence, player_x, term_sequence, player_y, term_sequence, player_z, term_sequence].concat();
                                let write = u8_vec.as_slice();
                                let _res = writestream.write_all(write).await;
                            },
                            //TODO: make other refresh packet types
                            _ => unreachable!()
                        }
                    },
                    Err(_e) => { println!("unable to recieve packet")}
                }
            }
        });
    }

    Ok(())
}

pub async fn batch_packets(rx: Receiver<Packet>) -> tokio::io::Result<Receiver<Vec<Packet>>> {
    let (batch_tx, batch_rx) = broadcast::channel(16);
    tokio::spawn(async move {
        let mut rx = rx;
        let mut last_tick = Instant::now();
        let mut packet_vec: Vec<Packet> = Vec::new();
        loop {
            if last_tick.elapsed().as_millis() >= 50 {
                let send_vec = packet_vec.to_owned();
                let _res = batch_tx.send(send_vec);
                packet_vec.clear();
                last_tick = Instant::now();
            }
            match rx.recv().await {
                Ok(packet) => {
                    packet_vec.push(packet);
                },
                Err(_e) => {
                    println!("error recieving packet")
                }
            }
        }
    });
    Ok(batch_rx)
}

pub fn handle_packet(packet: Packet, world: &mut World) {
    match packet.typ {
        PacketType::ServerReady => { print!("Packet: {:?}\n", packet.typ); },
        PacketType::Translate => {
            println!("translation packet recieved! creating netpacket entity...");
            world.create_entity().with(NetPacket { client_id: packet.client_id, action: Action::Translate(unsafe { packet.content.movement })}).build();
            println!("netpacket entity created.");
            print!("Packet: {:?}\n", packet.typ);
            //print!("Client: {}\n", packet.client_id);
            //print!("Packet Content:\n dx: {:?}\n dy: {:?}\n dz: {:?}\n", unsafe { packet.content.movement.dx }, unsafe { packet.content.movement.dy }, unsafe { packet.content.movement.dz });
        },
        PacketType::Attack => {
            world.create_entity().with(NetPacket { client_id: packet.client_id, action: Action::Attack(unsafe { packet.content.attack })}).build();
            print!("Packet: {:?}\n", packet.typ);
            //print!("Client: {}\n", packet.client_id);
        },
        PacketType::Pickup => {
            world.create_entity().with(NetPacket { client_id: packet.client_id, action: Action::Pickup(unsafe { packet.content.pickup })}).build();
            print!("Packet: {:?}\n", packet.typ);
            //print!("Client: {}\n", packet.client_id);
        },
        PacketType::Refresh => { 
            world.create_entity().with(NetPacket { client_id: packet.client_id, action: Action::Refresh(unsafe { packet.content.refresh })}).build();
            print!("Packet: {:?}\n", packet.typ);
            //print!("Client: {}\n", packet.client_id);
        },
        PacketType::SpawnPlayer => {
            spawn_player(world,
            unsafe { packet.content.spawn_player.position },
          unsafe { packet.content.spawn_player.health },
          unsafe{ packet.content.spawn_player.stats },
          unsafe { packet.content.spawn_player.player }
            );},
        PacketType::RefreshPosition => unreachable!()
    }
}

pub fn return_packet(world: &mut World, update_tx: &Sender<Packet>) -> Vec<Entity> {    
    let backpacket: ReadStorage<BackPacket> = world.system_data();
    let mut ent_vec: Vec<Entity> = Vec::new();
    for bp in backpacket.join() {
        let action = match bp.action {
            Action::Position(pos) => {
                pos
            }
            _ => unreachable!()
        };
        let packet = Packet {
            typ: PacketType::RefreshPosition,
            content: PacketContent { position: action },
            client_id: bp.player_id
            };
        let _update_res = update_tx.send(packet);
        ent_vec.push(bp.ent);
    }
    ent_vec
}

