use std::i32;

use specs::prelude::*;
use specs_derive::Component;

use crate::network::*;

#[derive(Debug, Clone, Copy)]
pub enum EntityType {
    Player,
    Npc,
    Object(ObjectType)
}

#[derive(Debug, Clone, Copy)]
pub enum ObjectType {
    Weapon,
    Armor,
    Consumable
}

#[derive(Debug, Clone, Copy)]
pub enum Stats {
    Strength,
    Constitution,
    Dexterity,
    Intelligence,
    Wisdom,
    Charisma,
}


#[derive(Component, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct Position {
    pub x: i32,
    pub y: i32,
    pub z: i32
}

#[derive(Component, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct Health {
    pub health: i32
}

#[derive(Component, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct TypeComponent {
    pub name: &'static str,
    pub typ: EntityType,
}

#[derive(Component, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct CharacterStats {
    pub level: i32,
    pub strength: i32,
    pub constitution: i32,
    pub dexterity: i32,
    pub intelligence: i32,
    pub wisdom: i32,
    pub charisma: i32
}

#[derive(Component, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct ItemStats {
    pub modifier: i32,
    pub modifies: Stats
}
#[derive(Component, Debug, Copy, Clone)]
#[storage(VecStorage)]
pub struct Player {
    pub client_id: i32,
}

#[derive(Component, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct Npc;

#[derive(Component, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct Item;

#[derive(Component, Debug, Clone, Copy)]
pub struct NetPacket {
    pub client_id: i32,
    pub action: Action
}
#[derive(Component, Debug, Clone, Copy)]
pub struct BackPacket {
    pub player_id: i32,
    pub action: Action,
    pub ent: Entity,
}

#[derive(Component, Debug, Clone, Copy)]
pub enum Action {
    Translate(MovementPacket),
    Attack(AttackPacket),
    Pickup(PickupPacket),
    Refresh(RefreshPacket),
    Position(PositionPacket),
}