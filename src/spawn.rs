use specs::prelude::*;
use crate::components::*;

pub fn spawn_player(world: &mut World, pos: Position, health: Health, stats: CharacterStats, player: Player) {
    world.create_entity().with(pos).with(health).with(stats).with(player).build();
}

pub fn spawn_npc(world: &mut World, pos: Position, health: Health, stats: CharacterStats) {
    world.create_entity().with(pos).with(health).with(stats).with(Npc).build();
}

pub fn spawn_item(world: &mut World, pos: Position, stats: ItemStats) {
    world.create_entity().with(pos).with(stats).with(Item).build();
}